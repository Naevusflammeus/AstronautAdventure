﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace AstronautGame.Classes
{
    class GameObjectCreator
    {
        public List<GameObject> Create(int amount, string name)
        {
            List<GameObject> createdObjects = new List<GameObject>();
            try
            {
                for(int i = 1; i < amount; i++)
                {
                    createdObjects.Add(new GameObject($"{name}{i.ToString()}"));
                }
                return createdObjects;
            }
            catch
            {
                throw new Exception($"Failed creating GameObject with amount:{amount} and ObjectName: {name}");
            }
        }

    }
}
