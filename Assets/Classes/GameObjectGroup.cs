﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace AstronautGame.Classes
{
    class GameObjectGroup
    {
        private Guid ID { get; }
        public string Name { get; set; }

        public List<GameObject> GameObjects { get; set; }

        private Transform Parent { get; set; }

        public GameObjectGroup(string name, Transform parentObject, List<GameObject> gameObjects)
        {
            try
            {
                ID = Guid.NewGuid(); 
                Name = name;
                Parent = parentObject;
                GameObjects = gameObjects;

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error Setting UID, Name and Parent Failed - {ex}");
            }

            ChangeParent();

        }

        public void ChangeParent()
        {
            if (GameObjects.Any())
            {

                foreach (GameObject gameObject in GameObjects)
                {
                    if (!sameParent(gameObject))
                    {
                        gameObject.transform.SetParent(Parent);
                    }
                }
            }
            else
            {
                throw new Exception($"Failed changing Parent");
            }
        }

        public void SetParent(Transform parent)
        {
            if (parent != null)
            {
                Parent = parent;
                ChangeParent();
            }
        }
        private bool sameParent(GameObject gameObject)
        {
            if (gameObject.transform.parent == null)
            {
                return false;
            }
            else if (gameObject.transform.parent.name == Parent.name)
            {
                return true;
            }
            else
            { 
                throw new Exception();
            }
        }

        public void createGameObjects(int Anzahl, string Name)
        {
            GameObjectCreator gameObjectCreator = new GameObjectCreator();
            List<GameObject> createdGameObjects = gameObjectCreator.Create(Anzahl, Name);
            foreach (GameObject gameObject in createdGameObjects)
            {
                GameObjects.Add(gameObject);
            }

            ChangeParent();
        }

        public void AddComponents<T>() where T : Component
        {
            foreach (GameObject gameObject in GameObjects)
            {
                try 
                {
                    gameObject.AddComponent<T>();
                }
                catch
                {
                    throw new Exception();
                }
            }
        }
    }

}
