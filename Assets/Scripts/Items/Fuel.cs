﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Editor:       Schneider Jan
//Erstellt:     31.03.2021
//Bearbeitet:   31.03.2021
//Status:       In Bearbeitung
public class Fuel : MonoBehaviour
{
    public float refillLevel;
    public GameObject fuel;

    public double maxUp;
    public double counter;

    bool countUp = true;
    double startY;
    double startX;
    double pushY;
    // Start is called before the first frame update
    void Start()
    {
        startY = fuel.transform.localPosition.y;    
        startX = fuel.transform.localPosition.x;
    }

    void FixedUpdate()
    {
        if (countUp && (pushY < maxUp))
            pushY += counter;        
        else
            countUp = false;

        if (!countUp && pushY > 0)
            pushY -= counter;
        else
            countUp = true; 

        fuel.transform.localPosition = new Vector2((float)startX, (float)startY + (float)pushY);
    }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.CompareTag("Player"))
        {
            other.SendMessage("RefillFuel",refillLevel);
            Destroy(gameObject);
        }

    }
}
