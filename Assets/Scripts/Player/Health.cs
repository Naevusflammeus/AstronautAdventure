﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

//Editor:       Schneider Jan
//Erstellt:     01.04.2021
//Bearbeitet:   10.04.2021
//Status:       In Bearbeitung
public class Health : MonoBehaviour
{
    public int maxLife;     //Maximale Leben (Erzeugt nach der Anzahl die Herzen
    public int life;        //Aktuelle Leben

    public GameObject parentPosition;   //Positions Game Object im Canvas an der Position an welcher die Herzen erzeugt werden sollen
    public Sprite lifeImage;            //Bild der Herzen
    public int shiftHorizontal;         //Versatz Horizontale Richtung

    public string nameOfObject;         //Name der Herzen

    public bool deleteHeart;
    public bool testBit;

    void Start()
    {
        for (int i = 0; i < maxLife; i++)
        {
            GameObject NewObj = new GameObject();                           //Neues Object erzeugen

            NewObj.name = nameOfObject + i.ToString();                      //Namen vergeben
            NewObj.AddComponent<Image>();                                   //Komponente Image hinzufügen
            NewObj.GetComponent<Image>().sprite = lifeImage;                //Herz Image einfügen

            NewObj.transform.parent = parentPosition.transform;             //In die Gruppe des Parent Object schieben

            Vector3 myPosition = parentPosition.transform.position;         //Aktuelle Position des Parent object in einer Vector3 Variable speichern
            myPosition.x += shiftHorizontal * i;                            //Verschiebung draufaddieren
            NewObj.transform.position = myPosition;                         //Gespeicherte & Geänderte Vector3 Variable an das Erzeugte Object übergeben
        }
    }

    void FixedUpdate()
    {      

        if (deleteHeart)
        {
            GameObject Test = GameObject.Find(nameOfObject + 2);
            Destroy(Test.gameObject);
            deleteHeart = false;
        }
    }


    public void getDamage(float damage)
    {
              



    }

}
