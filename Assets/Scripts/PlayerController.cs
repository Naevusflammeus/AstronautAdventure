using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AstronautGame.Classes;

public class PlayerController : MonoBehaviour
{
    public float maxSpeed = 4;
    public float jumpForce = 550;

    private Rigidbody2D rigidbody2D;
    private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        GameObject spawn = GameObject.Find("Spawnpunkt");
        GameObjectCreator ObjectCreator = new GameObjectCreator();
        GameObjectGroup HeartContainer = new GameObjectGroup("Heart", spawn.transform, ObjectCreator.Create(5, "Hearts"));
        HeartContainer.ChangeParent();
        HeartContainer.AddComponents<Image>();
    }

    

    // Update is called once per frame
    void Update()
    {
        
    }
}
